const mongoose = require('mongoose');

const { Schema } = mongoose;

const UserActivitySchema = new Schema({
  email: String,
  timeStamp: String,
  query: String,
  query_type: String,
  remarks: String,
  response: String
});

/*UsersSchema.methods.setPassword = function(password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};
*/

mongoose.model('UserActivity', UserActivitySchema);