const mongoose = require('mongoose');

const { Schema } = mongoose;

const DownloadActivitySchema = new Schema({
    email: String,
    timeStamp: String,
    query: String,
    query_type: String,
    totalNumberOfDaysRecords: String,
    totalBytesOfData: String,
    remarks: String,
    state_ref: String,
    date_ref: String,
    source_ref: String
});

/*UsersSchema.methods.setPassword = function(password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};
*/

mongoose.model('DownloadActivity', DownloadActivitySchema);