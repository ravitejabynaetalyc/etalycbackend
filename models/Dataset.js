const mongoose = require('mongoose');

const { Schema } = mongoose;

const DownloadsSchema = new Schema({
    id: String,
    name: String,
    region: String,
    data_Source: String,
    years: [Number],
    keywords: [String],
    text: String
});

/*UsersSchema.methods.setPassword = function(password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};
*/

mongoose.model('Dataset', DownloadsSchema);