const mongoose = require('mongoose');

const { Schema } = mongoose;

const DownloadsSchema = new Schema({
  email: String,
  timeStamp: String,
  expiration_time: String,
  query_status: String,
  query_request: String,
  is_data_present: String,
  dataSources: String,
  states: String,
  filenames: String,
  from_date: String,
  to_date: String,
  fname: String,
  remarks: String,
  body_data: String,
  downloadedFilenames: [String],
  createdAt: { type: Date}
});

/*UsersSchema.methods.setPassword = function(password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};
*/

mongoose.model('Downloads', DownloadsSchema);