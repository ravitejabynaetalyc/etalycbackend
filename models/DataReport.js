const mongoose = require('mongoose');

const { Schema } = mongoose;

const DataReportSchema = new Schema({
    date: Date,
    state: String,
    datasource: String,
    sizeInMB: Number
});

/*UsersSchema.methods.setPassword = function(password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};
*/

mongoose.model('DataReport', DataReportSchema);