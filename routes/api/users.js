const mongoose = require('mongoose');
const passport = require('passport');
const router = require('express').Router();
const auth = require('../auth');
const Users = mongoose.model('Users');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const generator = require('generate-password');
const UserActivity = mongoose.model('UserActivity');


const getToken = (req) => {
    let authorization = req.headers && req.headers.authorization;
    if(authorization){
      return jwt.decode(authorization, {complete: true })
      /*let authorization_arr = authorization.split(' ');
      console.log("authorization_arr ", authorization_arr)
      if(authorization_arr[0] === 'Bearer') {
        return jwt.decode(authorization_arr[1], {complete: true });
      }*/
    }
    return null;
};

const isAdmin= (dtoken)=> {
    if(dtoken && dtoken.payload.role==="admin"){
        return true;
    }
    return false;
};

const generatePassword = function(){
  let passwords = generator.generate({
    length: 10,
    uppercase: false
  });

  return passwords;
}


router.use(function (req, res, next) {
  // let query = req.protocol + '://' + req.get('host') + req.originalUrl;
  // let query_type = req.method;
  // let remarks = 'all activity';
  // let timeStamp = new Date().toGMTString();

  // console.log(fullUrl,req.method) 
  next()
})

//POST new user route (optional, everyone has access)
router.post('/register', auth.optional, (req, res, next) => {
  const { body: { user } } = req;

  if(!user.email) {
    return res.status(422).json({
      errors: {
        email: 'is required',
      },
    });
  }

  // if(!user.password) {
  //   return res.status(422).json({
  //     errors: {
  //       password: 'is required',
  //     },
  //   });
  // }


  let admin_username = getToken(req).payload.email;
  user.dateCreated = new Date();
  user.lastLoggedIn = "-";
  user.createdBy = admin_username;
  user.termsofuseaccepted = false;

  const finalUser = new Users(user);

  let password = generatePassword();

  finalUser.setPassword(password);

  return finalUser.save().then(() => {
    sendEmail(finalUser.email,admin_username,password,null, function(){
      res.json({ 
          user: finalUser.onSuccessfulRegister()
      })
    });
    

  }, () => {
    res.status(400);
    res.json({
        errors: {
            email: finalUser.email,
            message: "User with this email id already exists!"
        },
    })
  });
});





function sendEmail(user_email,admin_username,password, msgtype, callback){
  console.log("sendEmail: ",user_email,admin_username,password, msgtype, callback);


  console.log('Sending Email to User ' + user_email);


  let msg = '<p>Hi! You have been added as a user to ITS Heartland portal!</p><br/><br/><p>Username:</p>' + user_email + '<br/><p>Password:</p>' + password +'<br/><br/><p>Thank you!</p>';
  let subj = 'ITS Heartland: New User Created!';

  if(msgtype == 'changepassword'){
    subj = "ITS Heartland: New Password"
    msg = "Hi! "+ user_email +". <br/>Your password has been updated! <br/> New Password is " + password ;
  }

  if(msgtype == 'changepassword_user'){
    subj = "ITS Heartland: New Password"
    msg = "Hi! "+ user_email +". <br/>Your password has been updated!";
  }

   var transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'support@etalyc.com',
        pass: 'root@Etalyc123'
      }
    });

    var mailOptions = {
      from: 'support@etalyc.com',
      to: user_email,
      subject: subj,
      html: msg
    };

    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
        callback();
      }
    });
}


//POST login route (optional, everyone has access)
router.post('/login', auth.optional, (req, res, next) => {

  var query_url = req.protocol + '://' + req.get('host') + req.originalUrl;
  const { body: { user } } = req;

  if(!user.email) {
    return res.status(422).json({
      errors: {
        email: 'is required',
      },
    });
  }

  if(!user.password) {
    return res.status(422).json({
      errors: {
        password: 'is required',
      },
    });
  }

  return passport.authenticate('local', { session: false }, (err, passportUser, info) => {
    if(err) {
      return next(err);
    }
      logUserActivity(user.email,new Date(),query_url,'REQUEST LOGIN','')
    if(passportUser) {
      const user = passportUser;
      user.token = passportUser.generateJWT();
      console.log(user)
      user.recentLoggedIn = new Date();
      user.save().then(() => {
        //res.json({ user: user.toAuthJSON() });
        }, () => {
          
      });
      logUserActivity(user.email,new Date(),query_url,'RESPONSE LOGIN','','SUCCESSFUL');
      return res.json({ user: user.toAuthJSON() });
      //return 

      

      
    }
    logUserActivity(user.email,new Date(),query_url,'LOGIN','', 'FAILED 501: Incorrect Email or Password')
    res.status(501);
      return res.json({
              error: "Incorrect Email or Password"
          });
    
  })(req, res, next);
});



router.post('/acceptterms', auth.optional, (req, res, next) => {
    const token = getToken(req);
    
    const { body : { user }} = req;
    let email = token.payload.email;
    // if(token.payload.email!== email){
    //     return res.status(401).json({
    //         error: "Unauthorized Access"
    //     })
    // }

    let termsofuseaccepted = user.termsofuseaccepted;
    
    return Users.findOne({email})
        .then((finalUser)=>{
            if(!finalUser){
                res.status(422);
                return res.json({
                    error: email + "does not exist"
                });
            }

            finalUser.termsofuseaccepted = user.termsofuseaccepted;
            
            return finalUser.save().then(()=> {
              res.status(200)
              res.json({
                user: finalUser.getProfile()
              })
            });
        }, () => {
          res.status(422)
          res.json({
            error: "Error in Accept Terms"
        })
      });
});



//forgotPassword
//POST login route (optional, everyone has access)
router.post('/forgotpassword', auth.optional, (req, res, next) => {
  const { body: { user } } = req;

  if(!user.email) {
    return res.status(422).json({
      errors: {
        email: 'is required',
      },
    });
  }

  let password = generatePassword();
  let email = user.email;
  return Users.findOne({email})
    .then((user)=> {
        if(user){
            //user.isLogged =false;
            user.setPassword(password);
             return user.save().then(() => {
                sendEmail(user.email,null,password, text='changepassword', function(){
                  res.json({ 
                      msg: 'Password Changed Successfully'
                  })
                });
                

              }, () => {
                res.status(400);
                res.json({
                    errors: {
                        email: user.email,
                        message: "Error Occurred!"
                    },
                })
              });
        }

        res.status(422);
        return res.json({
            error: "Error! User " + email + " not found!"
        });
        }, () => res.json({error: "Error in logout"}));
});


router.get('/deleteaccount', auth.optional, (req, res, next) => {

    //let email = getToken(req).payload.email;
    const { query : { email }} = req;
    return Users.deleteOne({email})
        .then((success)=> {
            if(success){
                return res.json({
                    msg: "Successfully Deleted Account!"
                });
            }

            res.status(403);
            return res.json({
                error: "Error in deleting account. Please try again or contact admin. Thanks!"
            });
        }, () => 
          {
            res.status(403);
            return res.json({
                error: "Error in deleting account. Please try again or contact admin. Thanks!"
            });
          })
});


//LOGOUT
router.get('/logout', auth.optional, (req, res, next) => {
    var query_url = req.protocol + '://' + req.get('host') + req.originalUrl;
    let email = getToken(req).payload.email;
    console.log("USERNASME ", email)
    logUserActivity(email,new Date(),query_url,'REQUEST LOGOUT','');
    return Users.findOne({email})
        .then((user)=> {

          console.log(user)
            if(user){
                user.lastLoggedIn = user.recentLoggedIn;
                console.log(user.recentLoggedIn , user.lastLoggedIn)
                user.isLogged =false;

                return user.save().then(
                  ()=> {
                    logUserActivity(email,new Date(),query_url,'RESPONSE LOGOUT','', 'SUCCESSFUL')
                    res.json({
                      msg: "Successfully Logged Out"
                    })
                }
                  );
            }
            logUserActivity(email,new Date(),query_url,'RESPONSE LOGOUT','','FAILED')
            return res.json({
                error: email + "does not exist"
            });
        }, () => 
        {
          logUserActivity(email,new Date(),query_url,'RESPONSE LOGOUT','', 'FAILED')
          res.json({error: "Error in logout"})
        }

        );
});



// GET USER PROFILE
router.get('/profile', auth.optional, (req, res, next) => {
    const token = getToken(req);
    const email = token.payload.email;

    /*if(token.payload.email!==email){
        return res.status(401).json({
            error: "You don't have permission for this action"
        })
    }*/

    return Users.findOne({email})
        .then((user)=> {
            if(user){
                let obj = {};
                obj.name = user.name;
                obj.email = user.email;
                obj.role = user.role;
                obj.organization = user.organization;
                obj.dateCreated = user.dateCreated;
                obj.createdBy = user.createdBy;
                obj.lastLoggedIn = user.lastLoggedIn;
                obj.termsofuseaccepted = user.termsofuseaccepted;
                res.status(200);
                return res.json({
                    userinfo: obj
                });
            }
             res.status(422);
            return res.json({
                error: email + "does not exist"
            });
        }, () => res.json({error: "Fetch profile failed for user: "+ email}));
});


//UPDATE USER PROFILE

router.post('/updateprofile', auth.optional, (req, res, next) => {
    const token = getToken(req);
    
    const { body : { profile }} = req;
    let email = profile.email;
    console.log(token.payload)
    if(!(token.payload.email == email || token.payload.role == 'admin')){
        return res.status(401).json({
            error: "Unauthorized Access"
        })
    }
    
    return Users.findOne({email})
        .then((finalUser)=>{
            if(!finalUser){
                res.status(422);
                return res.json({
                    error: email + "does not exist"
                });
            }

            finalUser.name = profile.name;
            finalUser.organization = profile.organization;
            if(profile.password){
              finalUser.setPassword(profile.password);
            }

            if(profile.role){
              finalUser.role = profile.role;
            }

            return finalUser.save().then(()=> {
              if(profile.password){
                sendEmail(profile.email,null,profile.password, text='changepassword_user', function(){
                  res.json({ 
                      msg: 'Password Changed Successfully'
                  })
                });
              }

              res.json({
                user: finalUser.getProfile()
              })
            });
        }, () => {
          res.status(422)
          res.json({
            error: "Profile update failed"
        })
      });
});

//





//GET current route (required, only authenticated users have access)
router.get('/current', auth.required, (req, res, next) => {
  const { payload: { id } } = req;

  return Users.findById(id)
    .then((user) => {
      if(!user) {
        return res.sendStatus(400);
      }

      return res.json({ user: user.toAuthJSON() });
    });
});

router.get('/userlist', auth.optional, (req, res, next) => {
  return Users.find({}, {name:1,email: 1,organization: 1,role: 1,dateCreated: 1, createdBy: 1,lastLoggedIn: 1,  _id:0})
    .then((users) => {
      if(!users) {
        return res.sendStatus(400);
      }
      res.status(200)
      return res.json({ user: users });
    });
});

router.get('/deleteuser', auth.optional, (req, res, next) => {
  return Users.find()
    .then((users) => {
      if(!users) {
        return res.sendStatus(400);
      }

      return res.json({ user: users });
    });
});


function logUserActivity(email,timeStamp,query,query_type,remarks,response){


  const useractivity = {
    email: email,
    timeStamp: timeStamp,
    query: query,
    query_type: query_type,
    remarks: remarks,
    response: response
  }

  let finalUserActivity = new UserActivity(useractivity);

  return finalUserActivity.save().then(() => {
    console.log('activity saved : ', email,timeStamp,query,query_type)
  }, () => {
    console.log('activity NOT saved : ', email,timeStamp,query,query_type)
  });
}

module.exports = router;
module.exports.activity = logUserActivity;