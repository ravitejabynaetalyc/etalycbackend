const router = require('express').Router();
const fs = require('fs');
const mongoose = require('mongoose');
const Downloads = mongoose.model('Downloads');
const zlib = require('zlib');
const nodemailer = require('nodemailer');
const config = require('../../config/config');
const auth = require('../auth');
const jwt = require('jsonwebtoken');
const path = require('path');
const cron = require("node-cron");
let remarks = "";
const DownloadActivity = mongoose.model('DownloadActivity');
const users  = require('./users');
const query_type = require('../../config/query_type');


const getToken = (req) =>
{
    let authorization = req.headers && req.headers.authorization;
    if (authorization) {
        return jwt.decode(authorization, {complete: true})
        /*let authorization_arr = authorization.split(' ');
        console.log("authorization_arr ", authorization_arr)
        if(authorization_arr[0] === 'Bearer') {
          return jwt.decode(authorization_arr[1], {complete: true });
        }*/
    }
    return null;
}
;

var scheduleDeleteOfDownloadedFilesOlderThanTwoHours = function () {
    cron.schedule("*/1 * * * *", function () {
        console.log("running a task every minute");
        var downloadHistoryData = Downloads.find({$and :[{
            'createdAt': {$lt: new Date(Date.now() - 120 * 60 * 1000)} //120 * 60 * 1000
        }, {query_status: {$ne: ["Expired"]}}]}, {fname: 1, downloadedFilenames: 1}, function (err, docs) {
            if (err) {
                console.log("Error in getting docs");
            }
            console.log("Downlaod History : " + docs);
            let baseUrl = config.baseUrl;
            if (docs) {
                var documentsToBeDeleted = [];
                docs.forEach(data => {
                    documentsToBeDeleted.push(data.fname);
                try {
                    data.downloadedFilenames.forEach(file => {
                        var filename = baseUrl + data.fname + "/" + file + ".csv";
                        console.log(filename);
                        fs.unlinkSync(filename);

                    });
                    var filePath = baseUrl + data.fname;
                    fs.rmdirSync(filePath);

                    console.log('successfully deleted file : ' + filePath);


                } catch (err) {
                    // handle the error
                    console.log("Unable to delete files");
                }
            });
                Downloads.updateMany({'fname': {$in: documentsToBeDeleted}}, {$set: {query_status: 'Expired'}}, function (err) {
                    if (err) console.log("Unable to update docs to Expired");
                });

            }

        });

    });
}

var deleteFolderRecursive = function(path) {
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function(file, index){
            var curPath = path + "/" + file;
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};


router.get('/stream', function (req, res, next) {

    //let user_email = getToken(req).payload.email;
    let currentTimeStamp = new Date().getTime();
    const {query: {username}} = req;
    if(!username){
        res.status(422);
        return res.json({
            error: "username is required"
        });
    }
    var query_url = req.protocol + '://' + req.get('host') + req.originalUrl;
    users.activity(username, currentTimeStamp, query_url, "REQUEST STREAMING", "");
    const {query: {filename}} = req;
    if(!filename){
        res.status(422);
        return res.json({
            error: "filename is required"
        });
    }
    let baseUrl = config.baseUrl;
    let fpath = baseUrl + filename + '.csv';
    console.log('fpath while stream ',fpath);
    console.log(fpath);
    const state_datasource = filename.split('_');
    // fs.readdirSync(fpath).forEach(file => {
    fs.exists(fpath, function (exists) {
        if (exists) {
            console.log("[Streaming:] %s", filename);

            var rstream = fs.createReadStream(fpath);
            //res.setHeader('Content-disposition', 'attachment; filename='+filename+'.csv.gz');
            //res.setHeader('Content-type', 'application/x-gzip');
            let stat = fs.statSync(fpath);
            users.activity(username, currentTimeStamp, query_url, "RESPONSE STREAMING", "", "Download data Successful");
            logDownloadActivity(username, currentTimeStamp, query_url, "DOWNLOAD", 1, stat.size, "File to be downloaded : " + filename, state_datasource[0], "", state_datasource[1]);
            res.status(200);
            res.setHeader('Content-disposition', 'attachment; filename=' + filename + '.csv.gz');
            res.setHeader('Content-type', 'application/x-gzip');
            rstream.pipe(zlib.createGzip()).pipe(res);
            
        } else {
            //
            users.activity(username, currentTimeStamp, query_url, "RESPONSE STREAMING", "", "Download data Failed");
            logDownloadActivity(username, currentTimeStamp, query_url, "DOWNLOAD", 0, 0, "File to be downloaded : " + filename, state_datasource[0], "", state_datasource[1]);
            console.log("[ERROR Streaming:] %s", fpath);
            res.status(422);
            res.send("Error downloading file..");
            res.end();
            
            //
        }
    });
    // });
    // //console.log("Filenames : " + filenames);
    // for (const filename : filenames) {
    //
    // }


});


router.post('/getData', auth.optional, (req, res, next) => {
const {body: {downloadInfo}} = req;
let user_email = getToken(req).payload.email;
var query_url = req.protocol + '://' + req.get('host') + req.originalUrl;
var downloadInfoStr = JSON.stringify(downloadInfo);
/*if(!downloadInfo.email) {
  return res.status(422).json({
    errors: {
      email: 'is required',
    },
  });
}*/

if (!downloadInfo.dataSources) {
    return res.status(422).json({
        errors: {
            dataSources: 'is required',
        },
    });
}

if (!downloadInfo.states) {
    return res.status(422).json({
        errors: {
            states: 'is required',
        },
    });
}

if (!downloadInfo.filenames) {
    return res.status(422).json({
        errors: {
            filenames: 'is required',
        },
    });
}

if (!downloadInfo.from_date) {
    return res.status(422).json({
        errors: {
            from_date: 'is required',
        },
    });
}

if (!downloadInfo.to_date) {
    return res.status(422).json({
        errors: {
            to_date: 'is required',
        },
    });
}




downloadInfo.query_status = 'Pending';
let currentTimeStamp = new Date().getTime();
downloadInfo.timeStamp = currentTimeStamp;
downloadInfo.fname = 'download' + currentTimeStamp;
downloadInfo.email = user_email;
downloadInfo.query_request = query_url;
downloadInfo.body_data = downloadInfoStr;

const downloadInstance = new Downloads(downloadInfo);

console.log("Logging user activity for download");


return downloadInstance.save().then(() => {

console.log(downloadInfo.states);
console.log(downloadInfo.dataSources);
console.log(downloadInfo.filenames);
let fname = downloadInstance.fname + '.csv';

//var pipeEventStream = null;
var filesDoesNotExist = [];
var filesExist = [];

var dir = 'download' + currentTimeStamp;
//if (!fs.existsSync(dir)) {
//    fs.mkdirSync(dir);
//}
const totalNumberOfFiles = downloadInfo.states.length * downloadInfo.dataSources.length * downloadInfo.filenames.length;
let filesToBeProcessed = 0;
const downloadedFilenames = [];
let downloadBaseUrl = config.data_download_baseUrl;
dir =  downloadBaseUrl + dir;
if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}

for (let k = 0; k < downloadInfo.states.length; k++) {
    for (let j = 0; j < downloadInfo.dataSources.length; j++) {
        for (let l = 0; l < downloadInfo.filenames.length; l++) {

            //users.activity(user_email, currentTimeStamp, query_url, query_type.REQUESTDOWNLOAD, "Requested to download data");


            

            
            let fpath = downloadBaseUrl + downloadInfo.states[k].toLowerCase() + '\/' + downloadInfo.dataSources[j].toLowerCase() + '\/' + 'perdaydata' + '\/' + downloadInfo.filenames[l] + '.csv';
            let filenamedisplayed = downloadInfo.states[k].toLowerCase() + '_' + downloadInfo.dataSources[j].toLowerCase();
            let filename = downloadInfo.states[k].toLowerCase() + '_' + downloadInfo.dataSources[j].toLowerCase() + '_' + downloadInfo.filenames[l];
            logDownloadActivity(user_email, currentTimeStamp, query_url, 'GET_DATA_REQUEST',filesToBeProcessed, 0, fpath,downloadInfo.states[k].toLowerCase(), "",downloadInfo.dataSources[j].toLowerCase());
            if (fs.existsSync(fpath)) {
                if (!downloadedFilenames.includes(filenamedisplayed)) {
                    downloadedFilenames.push(filenamedisplayed);
                }
                filesExist.push(filename);
                filesToBeProcessed++;
            } else {
                filesDoesNotExist.push(filename);
            }
        }
    }
}
if (totalNumberOfFiles === filesDoesNotExist.length) {
    console.log("Total number of files requested : " + totalNumberOfFiles);
    console.log("Total number of files that does not exist " + filesDoesNotExist);
    downloadInstance.remarks = "None of the files exist: " + filesDoesNotExist;
    remarks = downloadInstance.remarks;
    sendEmail(downloadInstance, user_email)
} else {

    let filesProcessed = 0;
    try {
        for (let k = 0; k < downloadInfo.states.length; k++) {
            for (let j = 0; j < downloadInfo.dataSources.length; j++) {
                let filenamedisplayed = dir + '/' + downloadInfo.states[k].toLowerCase() + '_' + downloadInfo.dataSources[j].toLowerCase();
                for (let l = 0; l < downloadInfo.filenames.length; l++) {
                    let fpath = downloadBaseUrl + downloadInfo.states[k].toLowerCase() + '\/' + downloadInfo.dataSources[j].toLowerCase() + '\/' + 'perdaydata' + '\/' + downloadInfo.filenames[l] + '.csv';
                    logDownloadActivity(user_email, currentTimeStamp, query_url, 'GET_DATA_REQUEST',filesToBeProcessed, 0, fpath,downloadInfo.states[k].toLowerCase(), "",dataSources[j].toLowerCase());
                    fs.access(fpath, fs.constants.F_OK, function (err) {
                        // console.log(`${fpath} ${err ? 'does not exist' : 'exists'}`);
                        // });

                        // fs.exists(fpath, function (exists) {
                        if (!err) {
                            // filesProcessed++;
                            console.log("[Streaming:] %s", fpath);
                            let rstream = fs.createReadStream(fpath);

                            console.log('filenamedisplayed: ', filenamedisplayed);
                            let wstream = fs.createWriteStream(filenamedisplayed + '.csv', {'flags': 'a'})
                            //let pipeEventStream = rstream.pipe(zlib.createGzip()).pipe(wstream);

                            // let wstream = fs.createWriteStream(filenamedisplayed + '.csv', {'flags': 'a'})
                            let pipeEventStream = rstream.pipe(wstream);
                            console.log("Files Processed : " + filesProcessed + "filesTobe Processed " + filesToBeProcessed);
                            pipeEventStream.on('finish', function () {
                                filesProcessed++;
                                if (filesToBeProcessed === filesProcessed) {
                                    downloadInstance.downloadedFilenames = downloadedFilenames;
                                    console.log("Sending mail ");
                                    remarks = "Following files processed : " + filesExist;
                                    let stat = fs.statSync(dir);
                                    
                                    //email,timeStamp,query,query_type,totalNumberOfDaysRecords,totalBytesOfData,remarks, state_ref, date_ref, source_ref
                                    if (filesDoesNotExist.length !== 0) {
                                        remarks = remarks + ". \nFollowing Files do not exist: " + filesDoesNotExist;
                                    }
                                    let time_expiration = currentTimeStamp + config.expiration_time * 3600000
                                    downloadInstance.expiration_time = time_expiration;
                                    sendEmail(downloadInstance, user_email);
                                }


                            });
                        } else {
                            let filename = downloadInfo.states[k].toLowerCase() + '_' + downloadInfo.dataSources[j].toLowerCase() + '_' + downloadInfo.filenames[l];
                            console.log("\nFile does not exist : " + filename);
                            filesDoesNotExist.push(filename);

                        }
                        // if(!atLeastOneFileExists && (k==downloadInfo.states.length-1) && (l==downloadInfo.filenames.length-1) && (j==downloadInfo.dataSources.length-1)){
                        //     console.log(filesDoesNotExist + " doesn't exist")
                        //     sendEmail(downloadInstance,user_email)
                        // }

                    });


                }
                // let stat = fs.statSync(filenamedisplayed + '.csv');
                // logDownloadActivity(user_email, currentTimeStamp, query_url, 'DOWNLOAD_LOCAL',
                //     downloadInfo.dataSources[j].abbr, downloadInfo.states[k].abbr, "Files Processed",downloadInfo.filenames.length, stat.size, remarks);
            }


        }
    } catch (error) {
        //logDownloadActivity(user_email, currentTimeStamp, query_url, "GET_DATA_REQUEST", filename, filename, "Error Downloading files", 0, "");
    }
}

// if(!atLeastOneFileExists){
//   console.log(filesDoesNotExist + " doesn't exist");
//   sendEmail(downloadInstance,user_email);
// }


}

    ,() => res.json({
    errors: {
        message: "Failed to Process Query. Try Again!"
    },
}));
})
;


function sendEmail(downloadInstance, user_email) {
    console.log('Save in DB and Send Email to Person..');
    downloadInstance.query_status = 'Ready';
    downloadInstance.remarks = remarks;
    downloadInstance.createdAt = new Date();
    downloadInstance.save().then(() => {
        console.log('Query Status Saved');
},
    () =>
    {
        console.log('error in saving query status');
    }
)
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'support@etalyc.com',
            pass: 'root@Etalyc123'
        }
    });

    var mailOptions = {
        from: 'support@etalyc.com',
        to: user_email,
        subject: 'MCOMP Data Ready',
        text: 'Hi! Your data is ready. Please log into the portal to download the file! Thanks!'
    };
    // users.activity(user_email, currentTimeStamp, query_url, "DOWNLOAD RESPONSE", "Successfully scheduled download", "Your data is ready. Please log into the portal to download the file! Thanks!");
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}

router.get('/getDownloadHistory', auth.optional, function (req, res, next) {
    var query_url = req.protocol + '://' + req.get('host') + req.originalUrl;
    let user_email = getToken(req).payload.email;
    let currentTimeStamp = new Date().getTime();
    users.activity(user_email, currentTimeStamp, query_url, "DOWNLOAD HISTORY", "");
    return Downloads.find({
        'email': user_email
    }, {remarks: 1, downloadedFilenames: 1, expiration_time: 1, query_request: 1, query_status: 1, fname: 1, timeStamp: 1, body_data: 1, email: 1, from_date:1, to_date:1, _id: 0}).sort({timeStamp: -1})
        .then((downloads) => {
        if(!downloads) {
            return res.sendStatus(400);
        }
    res.status(200);
    return res.json({downloads: downloads});
    });
});


router.get('/getrealtimefeed/:state/:sourcetype', function (req, res, next) {
    // let user_email = getToken(req).payload.email;
    // let currentTimeStamp = new Date().getTime();
    // var query_url = req.protocol + '://' + req.get('host') + req.originalUrl;
    console.log(req.params)
    let state = req.params.state;
    let sourcetype = req.params.sourcetype;
    // users.activity(user_email, currentTimeStamp, query_url, "C2C", state + "|" + sourcetype);
    if (!state || !sourcetype) {
        res.status(400);
        res.json({
            'error': 'Url not valid'
        });
    }


    let baseUrl = config.data_download_baseUrl;
    let fpath = baseUrl + state + "/" + sourcetype + "/" + config.real_time_data_folder_name;
    console.log("fpath ", fpath)

    fs.exists(fpath, function (exists) {
        if (exists) {
            let flag = 0;
            fs.readdirSync(fpath).forEach(file => {

                var ext = path.extname(fpath + "/" + file);
            if (ext == '.xml') {
                flag = 1;
                console.log("[Streaming:] %s", fpath + "/" + file);
                var rstream = fs.createReadStream(fpath + "/" + file);
                res.setHeader('Content-disposition', 'attachment; filename=' + state + '_' + sourcetype + '.xml');
                res.setHeader('Content-type', 'application/xml');
                rstream.pipe(res);
                // users.activity(user_email, currentTimeStamp, query_url, "RESPONSE C2C", "Successfully Downloaded realtime data " + state + "|" + sourcetype);
            }

        });
            if (flag == 0) {
                console.log("[ERROR Streaming:] %s", fpath);
                res.status(400);
                // users.activity(user_email, currentTimeStamp, query_url, "RESPONSE C2C", "Error Downloading realtime data " + state + "|" + sourcetype);
                res.send("File Not Present..");
                res.end();
            }


        }
        else {
            console.log("[ERROR Streaming:] %s", fpath);
            res.status(400);
            // users.activity(user_email, currentTimeStamp, query_url, "RESPONSE C2C", "Error Downloading realtime data " + state + "|" + sourcetype);
            res.send("File Not Present..");
            res.end();
        }
    })


});

function logDownloadActivity(email,timeStamp,query,query_type,totalNumberOfDaysRecords,totalBytesOfData,remarks, state_ref, date_ref, source_ref){

    const downloadactivity = {
        email: email,
        timeStamp: timeStamp,
        query: query,
        query_type: query_type,
        totalNumberOfDaysRecords: totalNumberOfDaysRecords,
        totalBytesOfData: totalBytesOfData,
        remarks: remarks,
        state_ref: state_ref,
        date_ref: date_ref,
        source_ref: source_ref
    }

    let finalDownloadActivity = new DownloadActivity(downloadactivity);

    finalDownloadActivity.save().then(() => {
        console.log('activity saved : ', email,timeStamp,query,query_type)
    }, (err) => {
        console.log(err);
        console.log('activity NOT saved : ', email,timeStamp,query,query_type)
    });

}

module.exports = router;
module.exports.delete = scheduleDeleteOfDownloadedFilesOlderThanTwoHours;
