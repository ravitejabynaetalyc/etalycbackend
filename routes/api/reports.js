const router = require('express').Router();
const config = require('../../config/config');
const fs = require('fs');
const auth = require('../auth');
const users  = require('./users');
const mongoose = require('mongoose');
const DataReport = mongoose.model('DataReport');
const CollectionAttempt = mongoose.model('CollectionAttempt');
const DownloadActivity = mongoose.model('DownloadActivity');


Date.prototype.monthNames = [
    "January", "February", "March",
    "April", "May", "June",
    "July", "August", "September",
    "October", "November", "December"
];

Date.prototype.getMonthName = function() {
    return this.monthNames[this.getMonth()];
};
Date.prototype.getShortMonthName = function () {
    return this.getMonthName().substr(0, 3);
};


router.get('/getDataSize', auth.optional, function (req, res, next) {

    DataReport.aggregate([
        {
            $project: {
                year: {$year: "$date"},
                month: {$month: "$date"},
                dayOfMonth: {$dayOfMonth: "$date"},
                state:1,
                datasource:1,
                sizeInMB:1,
                numberOfDays: {
                    $cond: { if: { $gt: [ "$sizeInMB", 0 ] }, then: 1, else: 0 }
                }
            }
        },
        { $group: {
                "_id": {
                    "state": "$state",
                    "datasource": "$datasource",
                    "month": "$month",
                },
                "size": { "$sum": "$sizeInMB" },
                "numberOfDays": {"$sum": "$numberOfDays"}
            }}

    ]).sort({timeStamp: -1})
        .then((datareports) => {
            return res.json(datareports);
        if(!datareports) {
        return res.sendStatus(400);
    }


    return res.json({datareports: datareports});
});

});

router.post('/getDataSizeFilter', auth.optional, function (req, res, next) {

    const {body: {filterInfo}} = req;

    DataReport.aggregate([
        { $match : { $and : [ {state : {$in: filterInfo.statesArr}}, {datasource: {$in: filterInfo.dataSourcesArr}}]}},
        {
            $project: {
                year: {$year: "$date"},
                month: {$month: "$date"},
                dayOfMonth: {$dayOfMonth: "$date"},
                state:1,
                datasource:1,
                sizeInMB:1,
                numberOfDays: {
                    $cond: { if: { $gt: [ "$sizeInMB", 0 ] }, then: 1, else: 0 }
                }
            }
        },
        { $group: {
                "_id": {
                    "state": "$state",
                    "datasource": "$datasource",
                    "month": "$month",
                },
                "size": { "$sum": "$sizeInMB" },
                "numberOfDays": {"$sum": "$numberOfDays"}
            }}

    ]).sort({timeStamp: -1})
        .then((datareports) => {
        return res.json(datareports);
    if(!datareports) {
        return res.sendStatus(400);
    }

    return res.json({datareports: datareports});
});

});


router.get('/getCollectionAttempts', auth.optional, function (req, res, next) {

    CollectionAttempt.aggregate([
        {
            $project: {
                year: {$year: "$date"},
                month: {$month: "$date"},
                dayOfMonth: {$dayOfMonth: "$date"},
                state:1,
                datasource:1,
                status:1
            }
        },
        { $group: {
                "_id": {
                    "state": "$state",
                    "datasource": "$datasource",
                    "month": "$month",
                    "status": "$status"
                },
                "count": { "$sum": 1 }
            }}

    ]).sort({timeStamp: -1})
        .then((collectionAttempts) => {
            return res.json(collectionAttempts);
        if(!collectionAttempts) {
        return res.sendStatus(400);
    }


    return res.json({collectionAttempts: collectionAttempts});
});

});

router.post('/getCollectionAttemptsFilter', auth.optional, function (req, res, next) {
    const {body: {filterInfo}} = req;

    CollectionAttempt.aggregate([
        { $match : { $and : [ {state : {$in: filterInfo.statesArr}}, {datasource: {$in: filterInfo.dataSourcesArr}}]}},
        {
            $project: {
                year: {$year: "$date"},
                month: {$month: "$date"},
                dayOfMonth: {$dayOfMonth: "$date"},
                state:1,
                datasource:1,
                status:1
            }
        },
        { $group: {
                "_id": {
                    "state": "$state",
                    "datasource": "$datasource",
                    "month": "$month",
                    "status": "$status"
                },
                "count": { "$sum": 1 }
            }}

    ]).sort({timeStamp: -1})
        .then((collectionAttempts) => {
        return res.json(collectionAttempts);
    if(!collectionAttempts) {
        return res.sendStatus(400);
    }


    return res.json({collectionAttempts: collectionAttempts});
});

});

router.get('/getUserDownloadRequests', auth.optional, function (req, res, next) {

    DownloadActivity.aggregate([
        { $match : {query_type: {$eq: "GET_DATA_REQUEST"} }},
        {
            $project: {
                year: {$year: "$date"},
                month: {$month: "$date"},
                dayOfMonth: {$dayOfMonth: "$date"},
                state_ref:1,
                
                source_ref:1,
                email:1,

            }
        },
        { $group: {
                "_id": {
                    "state": "$state_ref",
                    "datasource": "$source_ref",
                    "status": "$status",
                    "email": "$email"
                },
                "count": { "$sum": 1 }
            }}

    ]).sort({timeStamp: -1})
        .then((userdownloadrequests) => {
            return res.json(userdownloadrequests);
        if(!userdownloadrequests) {
        return res.sendStatus(400);
    }


    return res.json({userdownloadrequests: userdownloadrequests});
});

});

router.post('/getUserDownloadRequestsFilter', auth.optional, function (req, res, next) {
    const {body: {filterInfo}} = req;
    DownloadActivity.aggregate([
        { $match : { $and : [ {query_type: {$eq: "GET_DATA_REQUEST"} }, {state_ref : {$in: filterInfo.statesArr}}, {source_ref: {$in: filterInfo.dataSourcesArr}}]}},
        {
            $project: {
                year: {$year: "$date"},
                month: {$month: "$date"},
                dayOfMonth: {$dayOfMonth: "$date"},
                state_ref:1,

                source_ref:1,
                email:1,

            }
        },
        { $group: {
                "_id": {
                    "state": "$state_ref",
                    "datasource": "$source_ref",
                    "status": "$status",
                    "email": "$email"
                },
                "count": { "$sum": 1 }
            }}

    ]).sort({timeStamp: -1})
        .then((userdownloadrequests) => {
        return res.json(userdownloadrequests);
    if(!userdownloadrequests) {
        return res.sendStatus(400);
    }

    res.sendStatus(200);
    return res.json({userdownloadrequests: userdownloadrequests});
});

});

router.get('/getUserDownloadResponses', auth.optional, function (req, res, next) {

    DownloadActivity.aggregate([
        { $match : {query_type: {$eq: "DOWNLOAD"} }},
        {
            $project: {
                year: {$year: "$date"},
                month: {$month: "$date"},
                dayOfMonth: {$dayOfMonth: "$date"},
                state_ref:1,
                source_ref:1,
                email:1,

            }
        },
        { $group: {
                "_id": {
                    "state": "$state_ref",
                    "datasource": "$source_ref",
                    "status": "$status",
                    "email": "$email"
                },
                "count": { "$sum": 1 }
            }}

    ]).sort({timeStamp: -1})
        .then((userdownloadresponses) => {
        return res.json(userdownloadresponses);
    if(!userdownloadrequests) {
        return res.sendStatus(400);
    }


    return res.json({userdownloadresponses: userdownloadresponses});
});

});

router.post('/getUserDownloadResponsesFilter', auth.optional, function (req, res, next) {
    const {body: {filterInfo}} = req;
    DownloadActivity.aggregate([
        { $match : { $and : [ {query_type: {$eq: "DOWNLOAD"} }, {state_ref : {$in: filterInfo.statesArr}}, {source_ref: {$in: filterInfo.dataSourcesArr}}]}},
        {
            $project: {
                year: {$year: "$date"},
                month: {$month: "$date"},
                dayOfMonth: {$dayOfMonth: "$date"},
                state_ref:1,
                source_ref:1,
                email:1,

            }
        },
        { $group: {
                "_id": {
                    "state": "$state_ref",
                    "datasource": "$source_ref",
                    "status": "$status",
                    "email": "$email"
                },
                "count": { "$sum": 1 }
            }}

    ]).sort({timeStamp: -1})
        .then((userdownloadresponses) => {
        return res.json(userdownloadresponses);
    if(!userdownloadrequests) {
        return res.sendStatus(400);
    }


    return res.json({userdownloadresponses: userdownloadresponses});
});

});




function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
};

module.exports = router;
