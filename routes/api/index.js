const express = require('express');
const router = express.Router();

router.use('/users', require('./users'));
router.use('/downloads', require('./downloads'));
router.use('/reports', require('./reports'));
router.use('/datasets', require('./datasets'));

module.exports = router;