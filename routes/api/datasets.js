const router = require('express').Router();
const auth = require('../auth');
const mongoose = require('mongoose');
const Dataset = mongoose.model('Dataset');

// GET USER PROFILE
router.post('/getDatasets', auth.optional, (req, res, next) => {
    const filters = req.body;
    console.log(filters);
    const regionsArr = filters.regions;
    const data_sourcesArr = filters.data_sources;
    const yearsArr = filters.years;
    const keywordsArr = filters.keywords;


    if ((Object.keys(filters).length === 0 && filters.constructor === Object) || (regionsArr.length === 0 && data_sourcesArr.length === 0 && yearsArr.length === 0 && keywordsArr.length === 0)) {
        console.log(filters);
        Dataset.find().then((datasets) => {
            if(!datasets){
                return res.sendStatus(400);
             }
            res.status(200);
            return res.json({datasetsArr: datasets});
        });
    } else {
        Dataset.find({$or: [{"region": {"$in": regionsArr}}, {"data_source": {"$in": data_sourcesArr}}, {"year": {"$in": yearsArr}}, {"keywords": {"$in": keywordsArr}}]}).then((datasets) => {
            if(!datasets){
                return res.sendStatus(400);
            }
            res.status(200);
        return res.json({datasetsArr: datasets});
        });
    }
});

module.exports = router;