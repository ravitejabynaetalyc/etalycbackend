  const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');
const cors = require('cors');
const mongoose = require('mongoose');
const errorHandler = require('errorhandler');
const fs = require('fs');

var https = require('https');
var http = require('http');


// var options = {
//   key: fs.readFileSync('/home/vmadmin/certificate_mcomp/mcomp-key.pem'),
//   cert: fs.readFileSync('/home/vmadmin/certificate_mcomp/mcomp-cert.pem')
// };

//Configure mongoose's promise to global promise
mongoose.promise = global.Promise;

//Configure isProduction variable
const isProduction = process.env.NODE_ENV === 'production';

//Initiate our app
const app = express();

//Configure our app
app.use(cors());
app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ secret: 'mcomp-auth', cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false }));


if(!isProduction) {
  app.use(errorHandler());
}

//Configure Mongoose
mongoose.connect('mongodb://localhost/mongo-db');
//mongoose.connect("mongodb://mcompuser:root%40123@10.13.1.3/mcomp-db", { 
//    uri_decode_auth: true 
//    }, function(err, db) {
//
//    }
//);
// mongoose.connect('mongodb://mcomp:root@199.127.138.10:27017/mcomp-db');

mongoose.set('debug', true);



//Models & routes
require('./models/Users');
require('./models/Downloads');
require('./config/passport');
require('./models/Useractivity');
require('./models/DownloadActivity');
require('./models/DataReport');
require('./models/CollectionAttempt');
require('./models/Dataset');
app.use(require('./routes'));

const downloads = require('./routes/api/downloads');
downloads.delete();

//Error handlers & middlewares
if(!isProduction) {

  app.use((req, res,err) => {

    res.status(err.status || 500);

    res.json({
      errors: {
        message: err.message,
        error: err,
      },
    });
  });
}

app.use((req, res,err) => {

  res.status(err.status || 500);

  res.json({
    errors: {
      message: err.message,
      error: {},
    },
  });
});

//app.listen(5000,'0.0.0.0', () => console.log('Server running on http://localhost:5000/'));

// Create an HTTP service.
module.exports = http.createServer(app).listen(5000);
// Create an HTTPS service identical to the HTTP service.
//https.createServer(options, app).listen(5001);