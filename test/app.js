/**

Test Cases

**/



var expect = require('chai').expect;
var app = require('../app');
var request = require('supertest');
var token = '';

//let's set up the data we need to pass to the login method
const userCredentials = {
	user : {
		email: 'lakshay.ahuja@etalyc.com', 
  		password: 'xweezvkoys'
	}
}


// describe('POST api/users/forgotpassword', function(done){
// //addresses 1st bullet point: if the user is logged in we should get a 200 status code
//   it('should return a 200 response if the user is logged in', function(done){

// 		authenticatedUser
// 	    .post('/api/users/forgotpassword')
// 	    .send({user: {email :'lakshay.ahuja@etalyc.com'}})
// 	    .end(function(err, response){
// 	      expect(200);
// 	    });
// 	    done();
//   });
// });


//now let's login the user before we run any tests
var authenticatedUser = request.agent(app);
before(function(done){
  authenticatedUser
    .post('/api/users/login')
    .send(userCredentials)
    .end(function(err, response){
      token = response.body.user.token;
      console.log(token)
      expect(response.statusCode).to.equal(200);
      done();
    });
});


//this test says: make a POST to the /login route with the email: sponge@bob.com, password: garyTheSnail
//after the POST has completed, make sure the status code is 200 
//also make sure that the user has been directed to the /home page

describe('GET api/users/profile', function(done){
//addresses 1st bullet point: if the user is logged in we should get a 200 status code
  it('should return a 200 response if the user is logged in', function(done){

    authenticatedUser.get('/api/users/profile')
    .set('Authorization', token)
    .expect(200, done);
  });
// //addresses 2nd bullet point: if the user is not logged in we should get a 302 response code and be directed to the /login page
//   it('should return a 501', function(done){
//     request(app).get('/api/users/profile')
//     .set('Authorization', token)
//     .expect(501, done);
//   });
});







describe('GET api/users/userlist', function(done){
//addresses 1st bullet point: if the user is logged in we should get a 200 status code
  it('should return a 200 response if the user is logged in', function(done){

    authenticatedUser.get('/api/users/userlist')
    .set('Authorization', token)
    .expect(200, done);
  });
});

//DELETE USER

describe('GET api/downloads/getDownloadHistory', function(done){
//addresses 1st bullet point: if the user is logged in we should get a 200 status code
  it('should return a 200 response if the user is logged in', function(done){

    authenticatedUser.get('/api/downloads/getDownloadHistory')
    .set('Authorization', token)
    .expect(200, done);
  });
});


describe('GET api/downloads/stream', function(done){
//addresses 1st bullet point: if the user is logged in we should get a 200 status code
  it('should return a 200 response if the user is logged in', function(done){

    authenticatedUser.get('/api/downloads/stream')
    .set('Authorization', token)
    .expect(422, done);
  });
});



describe('POST api/users/updateprofile', function(done){
//addresses 1st bullet point: if the user is logged in we should get a 200 status code
  it('should return a 200 response if the user is logged in', function(done){

		authenticatedUser
	    .post('/api/users/updateprofile')
	    .set('Authorization', token)
	    .send({profile: {name :'Laksh Ahuja', organization: 'Etlayc'}})
	    .end(function(err, response){
	      expect(200);
	    });
	    done();
  });
});




