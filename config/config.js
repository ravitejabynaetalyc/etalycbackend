var configuration = function(){};

const os = require('os');
let b_path = os.homedir();
configuration.prototype.mongourl = "fe";
configuration.prototype.baseUrl = b_path + '/etalyc/data/'
configuration.prototype.data_download_baseUrl = b_path + '/etalyc/data/';
configuration.prototype.perdaydata_folder_name = "perdaydata";
//configuration.prototype.ia_perday_url = '/home/etalyc/Lakshay/Projects/mcomp_backend/data/IA/Speed/perdaydata/';

configuration.prototype.real_time_data_folder_name = 'realtimedata';
configuration.prototype.expiration_time = 2;

//db.products.insert( { item: "card", qty: 15 } )
//db.products.insert(
// [
//     { _id: 11, item: "pencil", qty: 50, type: "no.2" },
//     { item: "pen", qty: 20 },
//     { item: "eraser", qty: 25 }
// ]
// )
//db.datasets.insert(
// [
//      {"id": "1","name": "IA Speed","region": "IA","data_source": "SPEED","years": [2018, 2019],"keywords": ["Iowa", "Speed"],"text": "This dataset contains the latest measurements taken in Dubuque, IA for their traffic signalized intersections."},
//
// ]
// )
var etalyc_datasets = [
    {
        "id": "1",
        "name": "IA_Speed",
        "geo_region_id": "1",
        "data_source": "1",
        "year": [2018, 2019],
        "keywords": ["Iowa", "Speed"],
        "text": "This dataset contains the latest measurements taken in Dubuque, IA for their traffic signalized intersections."
    },
    {
        "id": "2",
        "name": "IA_Incident",
        "geo_region_id": "1",
        "data_source": "2",
        "year": [2018, 2019],
        "keywords": ["Iowa", "Incident"],
        "text": "This dataset contains the latest measurements taken in Dubuque, IA for their traffic signalized intersections."
    },
    {
        "id": "3",
        "name": "IA_Location",
        "geo_region_id": "1",
        "data_source": "3",
        "year": [2018, 2019],
        "keywords": ["Iowa", "Location"],
        "text": "This dataset contains the latest measurements taken in Dubuque, IA for their traffic signalized intersections."
    },
    {
        "id": "4",
        "name": "MO_Speed",
        "geo_region_id": "3",
        "data_source": "1",
        "year": [2018, 2019],
        "keywords": ["Mossuri", "Speed"],
        "text": "This dataset contains the latest measurements taken in Dubuque, IA for their traffic signalized intersections."
    },
    {
        "id": "5",
        "name": "MO_Incident",
        "geo_region_id": "3",
        "data_source": "2",
        "year": [2018, 2019],
        "keywords": ["Mossuri", "Incident"],
        "text": "This dataset contains the latest measurements taken in Dubuque, IA for their traffic signalized intersections."
    },
    {
        "id": "6",
        "name": "MO_Location",
        "geo_region_id": "3",
        "data_source": "3",
        "year": [2018, 2019],
        "keywords": ["Mossuri", "Location"],
        "text": "This dataset contains the latest measurements taken in Dubuque, IA for their traffic signalized intersections."
    },
    {
        "id": "7",
        "name": "OK_Dms",
        "geo_region_id": "5",
        "data_source": "4",
        "year": [2018, 2019],
        "keywords": ["Oklahoma", "Dms"],
        "text": "This dataset contains the latest measurements taken in Dubuque, IA for their traffic signalized intersections."
    },
    {
        "id": "8",
        "name": "KS_Speed",
        "geo_region_id": "4",
        "data_source": "1",
        "year": [2018, 2019],
        "keywords": ["Kansas", "Speed"],
        "text": "This dataset contains the latest measurements taken in Dubuque, IA for their traffic signalized intersections."
    },
    {
        "id": "9",
        "name": "KS_Incident",
        "geo_region_id": "4",
        "data_source": "2",
        "year": [2018, 2019],
        "keywords": ["Kansas", "Incident"],
        "text": "This dataset contains the latest measurements taken in Dubuque, IA for their traffic signalized intersections."
    },
    {
        "id": "10",
        "name": "KS_Dms",
        "geo_region_id": "4",
        "data_source": "3",
        "year": [2018, 2019],
        "keywords": ["Kansas", "Dms"],
        "text": "This dataset contains the latest measurements taken in Dubuque, IA for their traffic signalized intersections."
    },
    {
        "id": "11",
        "name": "KS_CctvDetails",
        "geo_region_id": "4",
        "data_source": "4",
        "year": [2018, 2019],
        "keywords": ["Kansas", "CctvDetails"],
        "text": "This dataset contains the latest measurements taken in Dubuque, IA for their traffic signalized intersections."
    },
    {
        "id": "12",
        "name": "KS_CctvStatus",
        "geo_region_id": "4",
        "data_source": "5",
        "year": [2018, 2019],
        "keywords": ["Kansas", "CctvStatus"],
        "text": "This dataset contains the latest measurements taken in Dubuque, IA for their traffic signalized intersections."
    },
    {
        "id": "13",
        "name": "KS_RegionStatus",
        "geo_region_id": "4",
        "data_source": "6",
        "year": [2018, 2019],
        "keywords": ["Kansas", "RegionStatus"],
        "text": "This dataset contains the latest measurements taken in Dubuque, IA for their traffic signalized intersections."
    },
    {
        "id": "14",
        "name": "KS_FailureDetail",
        "geo_region_id": "4",
        "data_source": "7",
        "year": [2018, 2019],
        "keywords": ["Kansas", "FailureDetail"],
        "text": "This dataset contains the latest measurements taken in Dubuque, IA for their traffic signalized intersections."
    }

]

configuration.prototype.datasets = etalyc_datasets;

module.exports = new configuration();

