var query_type = {
    REGISTER: "register",
    LOGIN: "login",
    FORGOTPASSWORD: "forgot password",
    DELETEACCOUNT: "delete account",
    LOGOUT: "logout",
    PROFILE: "profile",
    UPDATEPROFILE: "updateprofile",
    STREAM: "stream",
    REQUESTDOWNLOAD: "request download",
    C2C: "c2c",
    DOWNLOADHISTORY: "download history"
};

module.exports = query_type;